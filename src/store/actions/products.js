import axios from 'axios'
import { FETCH_PRODUCT_LIST, FETCH_SEARCHED_PRODUCT } from '../constans';
const BASE_URL = process.env.REACT_APP_API_URL

export const fetchProductList = () => {
  return async dispatch => {
    await axios.get(`${BASE_URL}/products`).then(value => {
      dispatch({
        type: FETCH_PRODUCT_LIST,
        payload: value.data
      });
    });
  };
}
//random ürün listesi
export const fetchProductSuggestion = () => {
  return async dispatch => {
    await axios.get(`${BASE_URL}/products`).then(value => {
      dispatch({
        type: "FETCH_PRODUCT_LIST_SUGGESTION",
        payload: (value.data.sort(() => Math.random() - 0.5)).splice(1, 10)
      });
    });
  };
}

export const fetchProductFilter = (catName) => {
  return async dispatch => {
    dispatch({
      type: "FETCH_PRODUCT_FILTER",
      payload: catName
    });
  };
}

export const fetchSearchedProduct = (searchedProduct) => {
  return async dispatch => {
    await axios.get(`${BASE_URL}/products?name=${searchedProduct}`).then(value => {
      dispatch({
        type: FETCH_SEARCHED_PRODUCT,
        payload: value.data
      });
    });
  };
}

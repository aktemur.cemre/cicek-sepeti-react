import {combineReducers} from 'redux'
import products from './products'
import categories from './categories'
import basket from './basket'
import previousOrder from './previousOrder'

export default combineReducers({
  products,
  categories,
  basket,
  previousOrder
})
import { Route } from 'react-router-dom';

//Pages
import Header from './components/shared/header.jsx'
import Footer from './components/shared/footer.jsx'
import productsPage from './pages/products.jsx'


//Global css
import 'bootstrap/dist/css/bootstrap.css';
import './assets/global.scss'

function App() {

  return (
    <div className="App">
      <Route path="/" component={Header}></Route>
      <Route exact path='/' component={productsPage}></Route>
      <Route exact path='/urunler' component={productsPage}></Route>
      <Route path="/" component={Footer}></Route>
    </div>
  );
}

export default App;

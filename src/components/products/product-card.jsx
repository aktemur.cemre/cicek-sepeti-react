import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import 'react-lazy-load-image-component/src/effects/blur.css';

//Actions
import { fetchAddBasket, fetchBasketItemActionCount } from '../../store/actions/basket'

let basketList
const ProductCard = ({ product }) => {
  basketList = useSelector(state => state.basket.basketList)
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const [showCount, setShowCount] = useState(false)
  const [addBasketText, setAddBasketText] = useState("Sepete Ekle")
  let [productCount, setProductCount] = useState(1)

  //sepete ekle
  async function addBasket() {
    await dispatch(fetchAddBasket(product, product.fixedPrice));
    localStorage.setItem("basket", JSON.stringify(basketList));
  }
  //Ürün adedi arttır / azalt
  function actionCount(productCount, productId) {
    let newPrice = product.fixedPrice * productCount
    dispatch(fetchBasketItemActionCount(newPrice, productCount, productId));
    document.body.classList.add("add-basket");
    setTimeout(() => {
      document.body.classList.remove("add-basket");
    }, 500);
  }

  useEffect(() => {

    const getBasketData = basketList.find(item => item.product.id == product.id) //seçili ürünün adet bilgisini getir.
  });

  useEffect(() => {
    setLoading(false)
    setAddBasketText('Sepete Ekle');
  }, [basketList]);

  return (
    <>
      {product != null &&
        <div className="product-list__cell">
          <div className={`product-list__item d-flex flex-column justify-content-between ${showCount ? "selected" : ""}`}>
            <>

              <img loading="lazy" src={product.image} effect="black-and-white" alt={product.name} />


              <h2 className="product-list__name mt-2" alt={product.name}>{product.name}</h2>
            </>
            <>
              <p className="product-list__price mb-1">{(product.fixedPrice).toFixed(2)}₺</p>
              {!showCount ?
                <div className="button product-list__add-basket d-flex-center" onClick={() => {
                  setAddBasketText('');
                  setLoading(true);
                  setTimeout(() => {
                    addBasket();
                    setShowCount(true)
                    dispatch({ type: 'BASKET_LIST_OPEN', payload: true })
                  }, 2000)
                }}>
                  <div className={`spinner-border position-absolute ${!loading ? "d-none" : ""}`} role="status"></div>
                  <p>{addBasketText}</p>
                </div>
                :
                <div className="quantity d-flex w-100">
                  <p className="quantity-action" onClick={() => {
                    if (productCount != 1) {
                      setProductCount(productCount - 1);
                      actionCount(productCount - 1, product.id)
                    }
                  }}>
                    -</p>
                  <p className="quantity-value text-center">{productCount}</p>
                  <p className="quantity-action" onClick={() => {
                    if (productCount != 6) {
                      setProductCount(productCount + 1);
                      actionCount(productCount + 1, product.id)
                    }
                  }}>
                    +</p>
                </div>
              }
            </>
          </div>
        </div>
      }
    </>
  );
};

export default ProductCard;
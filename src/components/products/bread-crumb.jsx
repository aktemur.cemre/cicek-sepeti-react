import React from 'react';
import '../../assets/scss/layout/bread-crumb.scss'

const BreadCrumb = () => {
  return (
    <div className="bread-crumb d-flex align-items-center">
      <div className="custom-container">
        <p className="bread-crumb__text">ÇiçekSepeti Market{'>'}
          <span className="bread-crumb__text"> İstanbul</span>
          {'>'} <span className="bread-crumb__text--active">CicekSepetiBreadCrumb </span> </p>
      </div>
    </div>
  );
};

export default BreadCrumb;
import React from 'react'

const FooterCopyRight = () => {
    return (
      <div className="footer-copyright">
          <p className="footer-copyright__text">Copyright © 2019 Çiçek Sepeti İnternet Hizmetleri A.Ş</p>
      </div>
    )
  }

export default FooterCopyRight;

# Cicek Sepeti React

React-Redux ve hooks kullanarak Çiçek sepeti'nin anasayfasını tamamladım.

https://cicek-sepeti-cemre-aktemur.netlify.app/

Fonksiyonel Özellikleri;

- Header Ürün aramada inputa 3 karakter girdikten sonra çalışmakta.

- Headerda bulunan “Sepetim” tutarı 500 TL olduğunda altındaki barın tam
dolması ve textin “Kargonuz Bedava” olarak güncellenmesi
gerekmektedir. (250 TL tutarında bir sepet var ise barın %50’si dolmalıdır)

-  Sayfa ilk açıldığında “Tüm Kategoriler”in seçili gelmesi ve en az 20 adet
ürün olması gerekmektedir. (Ürünler klonlanabilir.)

- Kategorilerde seçim yapıldıktan sonra ilgili ürünler açık kalmalı diğer
ürünler kapalı olmalıdır.
